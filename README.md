# Linux on QEMU-emulated RISC-V 64bits architecture


## Description
The aim of this project is to explore and document the implementation of a riscv64 version of the GNU/Linux environment on a QEMU-emulated environment. This exploratory project will be based on the qemu-system-riscv64 emulator.


## Project status
Currently, the project contains howtos for installing Fedora 2020 and Ubuntu Live Server on a RISC-V architecture emulated by QEMU.

You can also find currently unsuccessful procedures for : networking two emulated machines (other than in NAT mode), providing a graphical display (VNC or Spice) and associated drivers, and managing the various virtual machines with `virt-manager` (libvirt) after using `virt-install` to create them.

_The timeframe associated with this school project has now ended. The aim is to make it accessible to people who would like to do similar installations and go further._

## Contributing
If you want to contribute to this project, we advise you to fork rather than contacting us to access this repository because we won't necessarily be available to come back on this project.

## License
 <p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Attribution 4.0 International <img style="height:10px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" height=25px> <img style="height:10px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1" height=25px></a></p> 

For reference, our current working system is Fedora OS. The same method will be working whether you're on any Linux distribution as an actual OS or inside a VM, or even if you're in an WSL
(Windows Subsystem for Linux) in your Windows machine ( 10 pro or above).
If you're an WSL user, please refer to the next tutorial for proper installation before running qemu.

This first part of the installation is taken from the official qemu website (https://www.qemu.org/download/) with a small modification : We will not run the original configuration command but rather an adapted one inspired from the fedora official wiki. 
Here's a step by step howto:

### Before starting the installation and configuration, make sure your system is up to date. This command might differ from one distribution to another, so make sure you find the one that works for you. If tou are in a Fedora OS:

```
dnf update -y | dnf upgrade
```

### You might also need certain dependencies. if you find any problems later, a quick troubleshooting will do the trick
#### List of dependencies:
```
    dnf install ninja-build 
    dnf install pkg-config
    dnf install libglib2.0-dev
    dnf install libpixman-1-dev
```


## Download qemu’s last version. W're currently on the version 8.2.0
wget https://download.qemu.org/qemu-8.2.0.tar.xz

# Unzip it: 
```
tar xvJf qemu-8.2.0.tar.xz
cd qemu-8.2.0
```

# Download Fedora prebuild images :
https://dl.fedoraproject.org/pub/alt/risc-v/repo/virt-builder-images/images/

### in our case, we will need the kernel and the filesystem (the bootloader): 
- Bootloader (kernel):  loads the operating system (fedora img) into the computer's memory during the system startup. In our case, we're using the last version (2020): 
    ```
    wget https://dl.fedoraproject.org/pub/alt/risc-v/repo/virt-builder-images/images/Fedora-Minimal-Rawhide-20200108.n.0-fw_payload-uboot-qemu-virt-smode.elf
    ```

- Root Filesystem: This is the top-level directory structure of the operating system. The associated verrion with .elf bootloader is:
    ```
    wget https://dl.fedoraproject.org/pub/alt/risc-v/repo/virt-builder-images/images/Fedora-Minimal-Rawhide-20200108.n.0-sda.raw.xz
    ```

- As you can see, the Filesystem is in a compressed format, you can easily unzip it:
    ```
    unxz Fedora-Minimal-Rawhide-*-sda.raw.xz
    ```
# Build and install: 
```

 ../configure --target-list=riscv64-softmmu
  cd ~/qemu8.2.0/build
  make
```

# check for the version:
```
qemu-system-riscv64 --version
```

### You can also download the CHECKSUM file for verification, to make sure it has not been tampered with:
```
wget https://dl.fedoraproject.org/pub/alt/risc-v/repo/virt-builder-images/images/Fedora-Developer-Rawhide-20200108.n.0-fw_payload-uboot-qemu-virt-smode.elf.CHECKSUM
```
### Now verify the downloads with the sha256sum command. Note the checksums reported, shown underlined here:
```
sha256sum 'Fedora-Developer-Rawhide-20200108.n.0-fw_payload-uboot-qemu-virt-smode.elf'
```
5ebc762df148511e2485b99c0bcf8728768c951680bc97bc959cae4c1ad8b053  Fedora-Developer-Rawhide-20200108.n.0-fw_payload-uboot-qemu-virt-smode.elf

```
cat 'Fedora-Developer-Rawhide-20200108.n.0-fw_payload-uboot-qemu-virt-smode.elf.CHECKSUM'
```
SHA256 (Fedora-Developer-Rawhide-20200108.n.0-fw_payload-uboot-qemu-virt-smode.elf) = 5ebc762df148511e2485b99c0bcf8728768c951680bc97bc959cae4c1ad8b053

## All what's left do do now is boot our risc-v with with Fedora: 
# The original booting script is this: 
```
./build/qemu-system-riscv64 \
   -nographic \
   -machine virt \
   -smp 4 \
   -m 2G \
   -kernel Fedora-Minimal-Rawhide-*-fw_payload-uboot-qemu-virt-smode.elf \
   -bios none \
   -object rng-random,filename=/dev/urandom,id=rng0 \
   -device virtio-rng-device,rng=rng0 \
   -device virtio-blk-device,drive=hd0 \
   -drive file=Fedora-Minimal-Rawhide-20200108.n.0-sda.raw,format=raw,id=hd0 \
   -device virtio-net-device,netdev=usernet \
   -netdev user,id=usernet,hostfwd=tcp::10000-:22
```

## Theoricaly, this should give you a first boot with root access and working network configuration. But, in our case, there was a problem with the netdev command so we switched to a tap interface:

```
./build/qemu-system-riscv64 \
   -nographic \
   -machine virt \
   -smp 4 \
   -m 2G \
   -kernel Fedora-Minimal-Rawhide-*-fw_payload-uboot-qemu-virt-smode.elf \
   -bios none \
   -object rng-random,filename=/dev/urandom,id=rng0 \
   -device virtio-rng-device,rng=rng0 \
   -device virtio-blk-device,drive=hd0 \
   -drive file=Fedora-Minimal-Rawhide-20200108.n.0-sda.raw,format=raw,id=hd0 \
   -netdev tap,id=usernet,ifname=tap0,script=no,downscript=no \
   -device virtio-net-device,netdev=usernet
```

This will give you a first boot but with no ping to 8.8.8.8. Try to play around with different troubleshootings
In our case: forcing the interface, restating the NetworkManager, manually configuring DHCP server didn't work. We tried switching to a bridged interface but still the same problem.
### Here's the script for the bridge unterface, for efficiency purposes, we're putting the script in a boot.sh with the execute permission. So the content of the file should be something like this:
```
cat boot.sh
```
```

#!/bin/bash
exec ./build/qemu-system-riscv64 \
   -nographic \
   -machine virt \
   -smp 4 \
   -m 2G \
   -kernel Fedora-Minimal-Rawhide-*-fw_payload-uboot-qemu-virt-smode.elf \
   -bios none \
   -object rng-random,filename=/dev/urandom,id=rng0 \
   -device virtio-rng-device,rng=rng0 \
   -device virtio-blk-device,drive=hd0 \
   -drive file=Fedora-Minimal-Rawhide-20200108.n.0-sda.raw,format=raw,id=hd0 \
   -device virtio-net-pci,netdev=usernet1,mac=52:54:00:12:34:56 \
   -netdev bridge,id=usernet1,br=virbr0

```

Later on we will be booting by using the command:
```
./boot.sh
```



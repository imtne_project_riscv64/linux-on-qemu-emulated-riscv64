# Step 0 : Requirements 

This howto starts from an Ubuntu 22.04.3 (LTS) machine. For information, we have it as a virtual machine in VirtualBox on Windows 10. You need an Internet connection (we are in NAT mode from the Windows 10 computer).


# Step 1 : Download QEMU & bootloader

Open the terminal and type in the following commands :
```shell
sudo apt install qemu-system-riscv64
sudo apt-get install qemu-system-misc opensbi u-boot-qemu
```
The following additional packages will be installed: `qemu-block-extra qemu-system-common qemu-system-data qemu-system-gui qemu-utils`


# Step 2 : Prepare and launch QEMU

I downloaded the installer at the following address: https://ubuntu.com/download/risc-v in the "QEMU Emulator" section. The resulting file is named ubuntu-22.04.3-live-server-riscv64.img.gz.
***Make sure you are in your work directory.***

To find out where to install the system, first create a disk (called "*disk*" here):

```
dd if=/dev/zero bs=1M of=disk count=1 seek=16383
```



Then unpack the image and launch QEMU with the VirtIO machine, the OpenSBI bios for 64-bit RISC-V, and the image as an installation disk/CD.

```shell
gzip -d ubuntu-22.04.3-live-server-riscv64.img.gz

qemu-system-riscv64 -machine virt -m 1G -smp cpus=1 -nographic -k fr \
    -bios /usr/lib/riscv64-linux-gnu/opensbi/generic/fw_jump.bin \
    -kernel /usr/lib/u-boot/qemu-riscv64_smode/u-boot.bin \
    -netdev user,id=net0 \
    -device virtio-net-device,netdev=net0 \
    -cdrom ./ubuntu-22.04.3-live-server-riscv64.img \
    -drive file=disk,format=raw,if=virtio \
    -device virtio-rng-pci
```
You can leave the `-nographic` property to test if the system starts well, but if you want to display QEMU in its own windows rather than in the terminal, you can remove it from the command.
The `-k fr` property stands for French keyboard. You can let the installer detect/suggest it by removing this property form the command too.

# Step 3 : Install Ubuntu

Now, you just need to follow the Ubuntu Server Live Installer. If it asks you to upgrade the installer to a more recent version, we advise you to do so.

![](https://framagit.org/imtne_project_riscv64/linux-on-qemu-emulated-riscv64/-/raw/main/images/installer_update.png)

If the installer pauses on a `Block probing` issue, please check in your QEMU command that your image is unpacked (.img) and included with its correct filepath in the parameter `-cdrom`.

For this project, we chose to use the entire disk *"disk"* rather than doing a `Custom storage layout`, and did not check the option that checks for third-parties drivers.

# Step 4 : Reboot your machine and launch Ubuntu Server

Once the installation is complete, the machine reboots and is supposed to display GRUB (so that we can choose 'Ubuntu'). If it's not GRUB but the installer that's displayed again, stop the machine manually and enter the following command (make sure you've removed the `--cdrom file=ubuntu-22...` line from Step 2 :

```shell
qemu-system-riscv64 -machine virt -m 1G -smp cpus=1 -nographic -k fr \
    -bios /usr/lib/riscv64-linux-gnu/opensbi/generic/fw_jump.bin \
    -kernel /usr/lib/u-boot/qemu-riscv64_smode/u-boot.bin \
    -netdev user,id=net0 \
    -device virtio-net-device,netdev=net0 \
    -drive file=disk,format=raw,if=virtio \
    -device virtio-rng-pci
```

# Step 5 : Use your emulated RISC-V Ubuntu Server

After having loaded the system, you can log in with the username and password you set at Step 3. Use `sudo -i` to get root access.

# Additional step : Set up a GUI (and eventually a desktop environment)



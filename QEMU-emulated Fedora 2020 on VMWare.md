### Reminder: This will work only for windows 10 pro or above, in our case, we’re working with Windows 11

## This is a step by step howto to get a working ubuntu subsystem inside your windows:
```
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
```

## Install ubuntu graphivally from your Microsoft store or by usnil command line:
```
wsl --install Ubuntu
```

## Check how many virtualization threads we have on the system. Any number above 0 should be fine.

```
egrep -c '(vmx|svm)' /proc/cpuinfo
```

## Check if all network services are up: 
virsh net-list –all
### If not active: 
```
sudo virsh net-autostart default
```

### Give all the permission to the user. In our case, we’re already root, but in case we were using a normal user: 
```
sudo usermod -aG libvirt $USER
sudo usermod -aG libvirt-qemu $USER
sudo usermod -aG kvm $USER
sudo usermod -aG input $USER
sudo usermod -aG disk $USER
```

### You can refer for the rest of semu installation and configuration to the other subfile. For now, you're good to go :)
